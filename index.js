// console.log("Hello World!");



// OBJECTS



/*

- An object is a data type that is used to represent real world object 

- It is a collection of related data and/or functionalities

- Information is stored in object represented in key: value pair 
	
	// key -> property of the object
	// value -> actual data to be stored


TWO WAYS in creating objects in JavaScript
1. Object loteral notation (let object = {})
2. Object Constructor notation 
	// Object Instantiation  (let object = new Object();)


*/






// ----------------------------------------------



// OBJECTS LITERAL NOTATION



let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log("Result from creating objects using initializer/literal notation: ");
console.log(cellphone);
console.log(typeof cellphone);



// "cellphone" - is called objectName
// "key" and "value" - is called properties of the objectName
// "name" and "manufacturDate" - is called the key
// "Nokia 3210" and "1999" - is called the value



// Another Example



let cellphone2 = {
	name: "Iphone 13",
	manufactureDate: 2021,
}

console.log(cellphone2);



// Another Example



let cellphone3 = {
	name: "Xiaomi 11t Pro",
	manufactureDate: 2021,
}

console.log(cellphone3);







// ----------------------------------------------


// OBJECT CONSTRUCTOR NOTATION


/*

- Creating objects using a constructor function

- Creates a re-usable function to create several objects that have the same data structure (blueprint)

- Syntax:
	function onjectName(keyA, keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

"this" 
- keyword refers to the properties within the object
- It allows the assignment of new object's properties by associating them with values received from the constructor function's parameter

*/



function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
}

let laptop1 = new Laptop("Lenovo", 2008);
console.log(laptop1);

let laptop2 = new Laptop("Macbook Air", 2020);
console.log(laptop2);


let laptop3 = new Laptop("Portal R2E CCMC", 1980);
console.log(laptop3);








// ----------------------------------------------



// OBJECT INSTANTATION


// CREATE AN EMPTY OBJECTS


/*

Syntax:
	let computer = {};

*/




let computer = {};
let myComputer = new Object();






// ----------------------------------------------


// ACCESSING OBJECT PROPERTIES / KEYS




// USING DOT NOTATION


console.log("Result from dot notation: " + laptop2.name);

console.log("Result from dot notation: " + laptop1.name);


console.log("Result from dot notation: " + laptop3.manufactureDate);








// ----------------------------------------------


// USING SQUARE BRACKET NOTATION



console.log("Result from square notation: " + laptop2["name"]);









// ----------------------------------------------


// ACCESSING ARRAY OF OBJECTS


/*

- Accessing object properties using the square bracket notation and array indexes can cause confusion.

*/





// let laptop1 = new Laptop("Lenovo", 2008);
// let laptop2 = new Laptop("Macbook Air", 2020);
// let laptop3 = new Laptop("Portal R2E CCMC", 1980);




let arrayObj = [laptop1, laptop2];
console.log(arrayObj[0]["name"]);



// Using dot notation naman (properties should have no space "manufactureDate")

// console.log("Result from dot notation: " + laptop2.name);


console.log(arrayObj[0].name);






// ----------------------------------------------


// INITIALIZING/ADDING/DELETING/RE-ASSIGNING OBJECT PROPERTIES



let car = {};
console.log("Current value of car object: ");
console.log(car);


// This is where you Initialize/adding object properties ⬇️

car.name ="Honda Civic";
console.log("Result from adding properties using dot notation");
console.log(car);







// ----------------------------------------------


// INITIALIZING/ADDING OBJECT USING SQUARE BRACKET NOTATION (NOT RECOMMENDED!)


// can create property with space "manufacture date"


car['manufacture date'] = 2019;
console.log("Result from adding properties using square bracket notation");
console.log(car);








// ----------------------------------------------


// DELETING OBJECT PROPERTIES



delete car['manufacture date'];
console.log("Result from deleting properties");
console.log(car);









// ----------------------------------------------


// RE-ASSIGNING OBJECT PROPERTY VALUES


car.name = "Toyota Vios";
console.log("Result from re-assigning property values: ");
console.log(car);








// ----------------------------------------------


// OBJECT METHODS



/*

- A method is function which is a property of an object

*/



let person = {
	name: "John",
	talk: function(){
		console.log("Hello my name is " + this.name);
	}
}

console.log(person);
person.talk();



// to access syntax: objectName.propertyName





// ANOTHER Example




let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas"
	},
	emails: ["joe@gmail.com", "joesmith@gmail.xyz"],
	introduce: function() {
		console.log("Hello my name is " + this.firstName + " " + this.lastName + "I live in " + this.address.city + ", " + this.address.country);
	}
}

friend.introduce();



// Another example




// creating 
person.walk = function(steps) {
	console.log(this.name + " walked " + steps + " steps forward");
}

console.log(person);
person.walk(50);






// ----------------------------------------------


// REAL WORLD APPLICATION OF OBJECTS


/*

Scenario:
	1. We would like to create a game that would have a several pokemon interact with each other.
	2. Every pokemon would have the same set of stats, properties and function.

	Stats:
	name
	level
	health = level * 2
	attack = level

*/




// CREATE AN OBJECT CONTRUCTOR TO LESSEN THE PROCESS IN CREATING THE POKEMON



function Pokemon(name, level) {
	this.name = name;
	this.level = level;
	this.health = level*2;
	this.attack = level;

	this.tackle = function(target) {
		console.log(this.name + 'tackled' + target.name);
		target.health -= this.attack;
		console.log("targetPokemon's health is now reduced to " + target.health)
	}

	this.faint = function() {
		console.log(this.name + "fainted");
	}
}

let pikachu = new Pokemon("Pikachu", 99);
console.log(pikachu);

let rattata = new Pokemon("Rattata", 5);
console.log(rattata);


pikachu.tackle(rattata);





